//
// Created by michael on 09.03.2020.
//

#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>


#include <iostream>
#include <vector>
#include <fstream>


class LabyrinthApp : public Application {
public:
    ShaderProgramPtr _shader;
    PersonCameraMoverPtr _inLabCamera;
    int cameraMode = 0;

    MeshPtr _labyrinth;
    MeshPtr _labyrinthNOROOF;
    float cell_size = 1.0;
    Map _labChar;

    void makeScene() override {
        Application::makeScene();

        std::ifstream file("699MartinsonData1/labirint_walls");
        std::string strinfo((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
        _labChar.emplace_back();
        int x = 0, y = 0;
        int start_x, start_y;
        for (char c : strinfo) {
            if (c == '\n') {
                _labChar.emplace_back();
                y += 1;
                x = 0;
                continue;
            }
            _labChar.back().emplace_back(c);
            if (c == '@') {
                start_x = x;
                start_y = y;
            }
            x += 1;
        }

        _cameraMover = std::make_shared<FreeCameraMover>();
        _inLabCamera = std::make_shared<PersonCameraMover>(1.0, 1.0, &_labChar);

        _shader = std::make_shared<ShaderProgram>("699MartinsonData1/shader.vert", "699MartinsonData1/shader.frag");



        _labyrinth = make_my_Labirint(1.0, true);
        _labyrinthNOROOF = make_my_Labirint(1.0, false);

        std::cout << "Start position : " << start_x << " " << start_y << "\n";
        _inLabCamera->setXYPosition(start_x * cell_size + cell_size / 2, start_y * cell_size + cell_size / 2);

        _labyrinth->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
        _labyrinthNOROOF->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
    }

    void updateGUI() override {
        Application::updateGUI();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_ESCAPE) {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            }
            if (key == GLFW_KEY_ENTER) {
                cameraMode = 1 - cameraMode;
            }
        }

        if (cameraMode == 0) {
            _inLabCamera->handleKey(_window, key, scancode, action, mods);
        } else {
            _cameraMover->handleKey(_window, key, scancode, action, mods);
        }
    }

    void handleMouseMove(double xpos, double ypos) override {
        glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        if (ImGui::IsMouseHoveringAnyWindow()) {
            return;
        }
        if (cameraMode == 0) {
            _inLabCamera->handleMouseMove(_window, xpos, ypos);
        } else {
            _cameraMover->handleMouseMove(_window, xpos, ypos);
        }
    }

    void update() override {

        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        if (cameraMode == 0) {
            _inLabCamera->update(_window, dt);
            _camera = _inLabCamera->cameraInfo();
        } else {
            _cameraMover->update(_window, dt);
            _camera = _cameraMover->cameraInfo();
        }
    }

    void draw() override {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        if (cameraMode == 0) {
            _shader->setMat4Uniform("modelMatrix", _labyrinth->modelMatrix());
            _labyrinth->draw();
        } else {
            _shader->setMat4Uniform("modelMatrix", _labyrinthNOROOF->modelMatrix());
            _labyrinthNOROOF->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);

    }
};

int main() {
    LabyrinthApp app;
    app.start();

    return 0;
}
